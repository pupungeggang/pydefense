import pygame

import UI
import functions as func
import variables as var

def display():
    var.screen.fill(UI.Colors.white)

def input_handle(event):
    if event.type == pygame.MOUSEBUTTONUP:
        mouse = pygame.mouse.get_pos()
        print(mouse)
        var.Game.scene = 'mode_select'
