import pygame

import variables as var
import title
import modeselect

def display():
    if var.Game.scene == 'title':
        title.display()
        
    elif var.Game.scene == 'mode_select':
        modeselect.display()
        
    pygame.display.flip()
