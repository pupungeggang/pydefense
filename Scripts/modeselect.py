import pygame

import UI
import variables as var
import functions as func

def display():
    var.screen.fill(UI.Colors.white)
    pygame.draw.rect(var.screen, UI.Colors.black, UI.ModeSelect.adventure, 2)
    pygame.draw.rect(var.screen, UI.Colors.black, UI.ModeSelect.extra, 2)

def input_handle(event):
    if event.type == pygame.MOUSEBUTTONUP:
        mouse = pygame.mouse.get_pos()
        
        if func.point_inside_rect(mouse, UI.ModeSelect.adventure):
            var.Game.scene = 'adventure_level_select'
            
        elif func.point_inside_rect(mouse, UI.ModeSelect.extra):
            var.Game.scene = 'extra_level_select'
