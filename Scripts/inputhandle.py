import sys
import pygame

import variables as var
import title
import modeselect

def input_handle():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
            
        elif var.Game.scene == 'title':
            title.input_handle(event)
            
        elif var.Game.scene == 'mode_select':
            modeselect.input_handle(event)
