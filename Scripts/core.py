import pygame

import inputhandle as inp
import display as dis
import variables as var

pygame.init()

clock = pygame.time.Clock()

while 1:
    frames = clock.tick(40)

    dis.display()
    inp.input_handle()
